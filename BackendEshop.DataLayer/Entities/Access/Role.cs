﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class Role : BaseEntity
    {
        #region Property

        [Display(Name = "RoleName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string RoleName { get; set; }

        [Display(Name = "RoleTitle")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string RoleTitle { get; set; }
        #endregion

        #region Relations
        public ICollection<UserRole> UserRoles { get; set; }

        #endregion
    }
}
