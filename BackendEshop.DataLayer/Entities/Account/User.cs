﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class User : BaseEntity
    {
        #region Properties
        [Display(Name = "UserName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserName { get; set; }

        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserFName { get; set; }

        [Display(Name = "LastName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserLName { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(500, ErrorMessage = "MaxLenght is {1}")]
        public string UserAddress { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserPhone { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserEmail { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserPassword { get; set; }

        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserActiveCode { get; set; }

        public bool UserIsActived { get; set; }
        #endregion

        #region Relations

        public ICollection<UserRole> UserRoles { get; set; }

        #endregion
    }
}
