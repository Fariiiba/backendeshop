﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class Slider : BaseEntity
    {
        #region Prop  
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string SliderTitle { get; set; }

        [Display(Name = "ImageName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(150, ErrorMessage = "MaxLenght is {1}")]
        public string SliderImageName { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(1000, ErrorMessage = "MaxLenght is {1}")]
        public string SliderDescription { get; set; }

        [Display(Name = "Link")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string SliderLink { get; set; }
        #endregion
    }
}