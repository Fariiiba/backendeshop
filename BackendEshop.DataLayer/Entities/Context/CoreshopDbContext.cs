﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BackendEshop.DataLayer
{
    public class CoreshopDbContext : DbContext
    {
        #region Constructor
        public CoreshopDbContext(DbContextOptions<CoreshopDbContext> options) : base(options)
        { }
        #endregion

        #region CascadeDatabase
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var cascade = modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetForeignKeys()).Where(fk => fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascade)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }

        #endregion

        #region Db set
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Slider> sliders { get; set; }
        public DbSet<Product> products { get; set; }
        public DbSet<ProductCategory> productCategories { get; set; }
        public DbSet<ProductGallery> productGalleries { get; set; }
        public DbSet<ProductSelectedCategory> productSelectedCategories { get; set; }
        public DbSet<ProductVisit> productVisits { get; set; }
        public DbSet<Search> search { get; set; }

        #endregion

    }
}