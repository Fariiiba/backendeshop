﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackendEshop.DataLayer
{
    public class ProductCategory : BaseEntity
    {
        #region Prop
        public long? ProductId { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Title { get; set; }
        #endregion

        #region Relations
        [ForeignKey(name: "ParentId")]
        public ProductCategory parentCategory { get; set; }
        public ICollection<ProductSelectedCategory> productSelectedCategories { get; set; }
        #endregion

    }
}
