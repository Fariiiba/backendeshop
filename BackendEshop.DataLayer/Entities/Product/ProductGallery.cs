﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class ProductGallery : BaseEntity
    {
        #region PropId
        public string ProductId { get; set; }
        #endregion
        #region Prop
        [Display(Name = "")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string ProductImageName { get; set; }
        #endregion
        #region Relations
        public Product product { get; set; }
        #endregion
    }
}
