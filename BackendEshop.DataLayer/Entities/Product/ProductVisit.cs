﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class ProductVisit : BaseEntity
    {
        #region Prop
        public long ProductId { get; set; }

        [Display(Name = "Ip")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserIp { get; set; }
        #endregion

        #region Relations
        public Product product { get; set; }
        #endregion
    }
}