﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class Product : BaseEntity
    {
        #region Prop
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string ProductName { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public int ProductPrice { get; set; }

        [Display(Name = "ShortDescription")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string ProductShortDesc { get; set; }

        [Display(Name = "LangDescription")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string ProductLangDesc { get; set; }

        [Display(Name = "ImageName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string ProductImageName { get; set; }

        [Display(Name = "Exists")]
        public bool IsExists { get; set; }

        [Display(Name = "Special")]
        public bool IsSpecial { get; set; }
        #endregion

        #region relations
        public ICollection<ProductGallery> productGalleries { get; set; }
        public ICollection<ProductVisit> productVisits { get; set; }
        public ICollection<ProductSelectedCategory> productSelectedCategories { get; set; }
        #endregion
    }
}