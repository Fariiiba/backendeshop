﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackendEshop.DataLayer
{
    public class ProductSelectedCategory : BaseEntity
    {
        #region Prop
        public long ProductId { get; set; }

        public long ProductCategoryId { get; set; }
        #endregion

        #region Relations

        public Product product { get; set; }
        public ProductCategory productCategory { get; set; }
        #endregion
    }
}