﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BackendEshop.DataLayer
{
    public class BaseEntity
    {
        [Key]
        public long ID { get; set; }
        public bool IsDelete { get; set; }
        public DateTime RecordDate { get; set; }
        public DateTime Modification { get; set; }
    }
}
