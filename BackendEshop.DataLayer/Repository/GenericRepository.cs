﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BackendEshop.DataLayer
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        #region ctor
        private CoreshopDbContext _dbContext;
        private DbSet<TEntity> _dbSet;

        public GenericRepository(CoreshopDbContext dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = this._dbContext.Set<TEntity>();
        }

        public async Task AddEntity(TEntity entity)
        {
            entity.RecordDate = DateTime.Now;
            entity.Modification = entity.RecordDate;
            await _dbSet.AddAsync(entity);
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }

        public IQueryable<TEntity> GetEntitiesQuery()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<TEntity> GetEntityById(long entityId)
        {
            return await _dbSet.SingleOrDefaultAsync(e => e.ID == entityId);
        }

        public void RemoveEntity(TEntity entity)
        {
            entity.IsDelete = true;
            _dbSet.Update(entity);
        }

        public async Task RemoveEntity(long entityId)
        {
            var entity = await GetEntityById(entityId);
            RemoveEntity(entity);
        }

        public async Task SaveChanges()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void UpdateEntity(TEntity entity)
        {
            entity.RecordDate = DateTime.Now;
            _dbSet.Update(entity);
        }
        #endregion


    }
}
