﻿using System.ComponentModel.DataAnnotations;

namespace BackendEshop.Core
{
    public class RegisterDto
    {

        [Display(Name = "UserName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string UserName { get; set; }

        [Display(Name = "FirstName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string FirstName { get; set; }

        [Display(Name = "LastName")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string LastName { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Address { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Password { get; set; }

        [Display(Name = "ConfirmPassword")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        [Compare("Password", ErrorMessage = "please Enter Repassword again")]
        public string ConfirmPassword { get; set; }


    }

}
