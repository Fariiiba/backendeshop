﻿using System.ComponentModel.DataAnnotations;

namespace BackendEshop.Core
{
    public class LoginDto
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please Enter {0}")]
        [MaxLength(100, ErrorMessage = "MaxLenght is {1}")]
        public string Password { get; set; }
    }
}
