﻿using BackendEshop.DataLayer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace BackendEshop.Core
{
    public static class ConnectionExtension
    {
        public static IServiceCollection AddApplicationDbContext(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddDbContext<CoreshopDbContext>(options =>
            {
                var connectionstring = "ConnectionStrings:BackendAPI_DBContext:Development";
                //with sql
                //options.UseSqlServer(configuration[connectionstring]);

                //with postgres
                options.UseNpgsql(configuration[connectionstring]);
            }
            );
            return service;
        }
    }
}