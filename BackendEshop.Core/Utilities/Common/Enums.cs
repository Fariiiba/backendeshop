﻿
namespace BackendEshop.Core
{
    public enum RegisterUserResult
    {
        Success,
        EmailExists
    }

    public enum LoginUserResult
    {
        Success,
        IncorrectData,
        NotActivated
    }

}
