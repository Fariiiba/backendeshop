﻿using System;
using System.Threading.Tasks;
using BackendEshop.DataLayer;

namespace BackendEshop.Core
{
    public interface IProductService : IDisposable
    {
        Task AddProdct(Product product);
        Task UpdateProdct(Product product);
    }
}

