﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;

namespace BackendEshop.Core
{
    public interface IUserService : IDisposable
    {
        Task<List<User>> GetAllUser();
        Task<RegisterUserResult> RegisterUser(RegisterDto register);
        bool IsUserExistsByEmail(string email);
        Task<LoginUserResult> LoginUser(LoginDto login);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserByUserId(long userId);
    }
}
