﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;


namespace BackendEshop.Core
{
    public interface ISearchService : IDisposable
    {
        Task<List<Search>> GetAllSearch();
        Task<List<Search>> GetActiveSearch();
        Task AddSearch(Search search);
        Task UpdateSearch(Search search);
        Task<SearchService> GetSearchById(long searchId);
    }
}
