﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;
using Microsoft.EntityFrameworkCore;

namespace BackendEshop.Core
{
    public class UserService : IUserService
    {

        #region ctor
        private IGenericRepository<User> _userRepository;
        private IPasswordHelper _passwordHelper;
        public UserService(IGenericRepository<User> userRepository, IPasswordHelper passwordHelper)
        {
            _userRepository = userRepository;
            _passwordHelper = passwordHelper;
        }
        #endregion

        #region UserSection
        public async Task<List<User>> GetAllUser()
        {
            return await _userRepository.GetEntitiesQuery().ToListAsync();
        }

        public async Task<RegisterUserResult> RegisterUser(RegisterDto register)
        {
            if (IsUserExistsByEmail(register.Email))
                return RegisterUserResult.EmailExists;

            var user = new User
            {
                UserName = register.UserName.SanitizeText(),
                UserFName = register.FirstName.SanitizeText(),
                UserLName = register.LastName.SanitizeText(),
                UserAddress = register.Address.SanitizeText(),
                UserPhone = register.Phone.SanitizeText(),
                UserEmail = register.Email.SanitizeText(),
                UserActiveCode = Guid.NewGuid().ToString(),
                UserIsActived = true,
                IsDelete = false,
                //   UserPassword = _passwordHelper.EncodePasswordMd5(register.Password),
                UserPassword = register.Password
            };

            await _userRepository.AddEntity(user);

            await _userRepository.SaveChanges();

            return RegisterUserResult.Success;
        }

        public bool IsUserExistsByEmail(string email)
        {
            return _userRepository.GetEntitiesQuery().Any(s => s.UserEmail == email.ToLower().Trim());
        }

        public async Task<LoginUserResult> LoginUser(LoginDto login)
        {
            //var password = _passwordHelper.EncodePasswordMd5(login.Password);
            var password = login.Password;

            var user = await _userRepository.GetEntitiesQuery()
                .SingleOrDefaultAsync(s => s.UserEmail == login.Email.ToLower().Trim() && s.UserPassword == password);

            if (user == null) return LoginUserResult.IncorrectData;

            if (!user.UserIsActived) return LoginUserResult.NotActivated;

            return LoginUserResult.Success;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _userRepository.GetEntitiesQuery().SingleOrDefaultAsync(s => s.UserEmail == email.ToLower().Trim());
        }

        public async Task<User> GetUserByUserId(long userId)
        {
            return await _userRepository.GetEntityById(userId);
        }
        #endregion

        #region dispose
        public void Dispose()
        {
            _userRepository?.Dispose();
        }
        #endregion

    }
}
