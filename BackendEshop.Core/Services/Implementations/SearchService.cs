﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;

namespace BackendEshop.Core
{
    public class SearchService : ISearchService
    {
        private IGenericRepository<Search> _searchRepository;
        #region ctor
        public SearchService(IGenericRepository<Search> searchRepository)
        {
            _searchRepository = searchRepository;
        }
        #endregion

        #region service
        public async Task AddSearch(Search search)
        {
            await _searchRepository.AddEntity(search);
            await _searchRepository.SaveChanges();
        }

        public void Dispose()
        {
            _searchRepository?.Dispose();
        }

        public Task<List<Search>> GetActiveSearch()
        {
            throw new NotImplementedException();
        }

        public Task<List<Search>> GetAllSearch()
        {
            throw new NotImplementedException();
        }

        public Task<SearchService> GetSearchById(long searchId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateSearch(Search search)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}