﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;
using Microsoft.EntityFrameworkCore;

namespace BackendEshop.Core
{
    public class SliderService : ISliderService
    {
        private IGenericRepository<Slider> _sliderRepository;
        #region ctor
        public SliderService(IGenericRepository<Slider> sliderRepository)
        {
            _sliderRepository = sliderRepository;
        }
        #endregion
        #region slider
        public void Dispose()
        {
            _sliderRepository?.Dispose();
        }

        public async Task<List<Slider>> GetActiveSliders()
        {
            return await _sliderRepository.GetEntitiesQuery().Where(s => !s.IsDelete).ToListAsync(); ;
        }

        public async Task<List<Slider>> GetAllSliders()
        {
            return await _sliderRepository.GetEntitiesQuery().ToListAsync();
        }

        public async Task<Slider> GetSliderById(long sliderId)
        {
            return await _sliderRepository.GetEntityById(sliderId);
        }

        public async Task AddSlider(Slider slider)
        {
            await _sliderRepository.AddEntity(slider);
            await _sliderRepository.SaveChanges();
        }
        public async Task UpdateSlider(Slider slider)
        {
            _sliderRepository.UpdateEntity(slider);
            await _sliderRepository.SaveChanges();
        }
        #endregion
    }
}
