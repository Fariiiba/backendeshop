﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendEshop.DataLayer;

namespace BackendEshop.Core
{
    public class ProductService : IProductService
    {
        #region ctor
        private IGenericRepository<Product> _productRepository;
        private IGenericRepository<ProductCategory> _productCategoryRepository;
        private IGenericRepository<ProductGallery> _productGalleryRepository;
        private IGenericRepository<ProductSelectedCategory> _productSelectedCategoryRepository;
        private IGenericRepository<ProductVisit> _productVisitRepository;

        public ProductService(IGenericRepository<Product> productRepository, IGenericRepository<ProductCategory> productCategoryRepository, IGenericRepository<ProductGallery> productGalleryRepository, IGenericRepository<ProductSelectedCategory> productSelectedCategoryRepository, IGenericRepository<ProductVisit> productVisitRepository)
        {
            _productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
            _productGalleryRepository = productGalleryRepository;
            _productSelectedCategoryRepository = productSelectedCategoryRepository;
            _productVisitRepository = productVisitRepository;

        }
        #endregion

        #region implement

        public async Task AddProdct(Product product)
        {
            await _productRepository.AddEntity(product);
            await _productRepository.SaveChanges();
        }
        public async Task UpdateProdct(Product product)
        {
            _productRepository.UpdateEntity(product);
            await _productRepository.SaveChanges();
        }

        public void Dispose()
        {
            _productRepository?.Dispose();
            _productCategoryRepository?.Dispose();
            _productGalleryRepository?.Dispose();
            _productSelectedCategoryRepository?.Dispose();
            _productVisitRepository?.Dispose();
        }
        #endregion

    }
}
