﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendEshop.Core;

namespace BackendEshop.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class UsersController : ControllerBase
    {
        #region ctor

        private IUserService _userService;
        public UsersController(IUserService userService)
        {
            this._userService = userService;
        }
        #endregion

        #region listUser
        //[HttpGet("GetUsers")]
        [HttpGet]
        public async Task<IActionResult> Users()
        {
            return new ObjectResult(await _userService.GetAllUser());
        }

        #endregion
    }
}