﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackendEshop.Core;
using BackendEshop.DataLayer;


namespace BackendEshop.Controllers
{   
    [ApiController]
    [Route("[controller]")]

    public class SearchController : BaseController
    {
        #region ctor

        private ISearchService _searchService;
        public SearchController(ISearchService searchService)
        {
            this._searchService = searchService;
        }
        #endregion


        // GET: api/<SearchController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<SearchController>/5
        [HttpGet("Getsearch")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SearchController>
        [HttpPost("Postsearch")]
        public async Task<IActionResult> Post([FromBody] Search search)
        {

            await _searchService.AddSearch(search);

            return JsonResponseStatus.Success();
        }

        // PUT api/<SearchController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SearchController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
