﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BackendEshop.Core;

namespace BackendEshop.Controllers
{  
    [ApiController]
    [Route("[controller]")]

    public class SliderController : BaseController
    {
        #region ctor

        private ISliderService _sliderService;
        public SliderController(ISliderService sliderService)
        {
            this._sliderService = sliderService;
        }
        #endregion

        #region listslider
        [HttpGet("GetActiveSlider")]
        public async Task<IActionResult> GetActiveSlider()
        {
            //Thread.Sleep(4000);
            var slider = await _sliderService.GetActiveSliders();
            return JsonResponseStatus.Success(slider);
        }

        #endregion
    }
}
